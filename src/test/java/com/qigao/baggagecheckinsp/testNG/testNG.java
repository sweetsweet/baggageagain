package com.qigao.baggagecheckinsp.testNG;

import com.qigao.baggagecheckinsp.controller.Calc;
import com.qigao.baggagecheckinsp.entity.Form;
import org.testng.Assert;
import org.testng.annotations.Test;
public class testNG {
    Calc calc=new Calc();
    @Test
    public void t1() {
        System.out.println("this is new test");
        Form form=new Form("","成人","s1","国内航班","v1","1",
                "10 10 10 10","普通行李","","","","","5000",-1);
        Assert.assertEquals(-1,calc.calculatePrice(form));
    }
    @Test
    public void t2() {
        System.out.println("this is new test");
        Form form=new Form("","成人","s1","国内航班","v1","1",
                "100 100 100 10","普通行李","","","","","5000",-1);
        Assert.assertEquals(-1,calc.calculatePrice(form));
    }
    @Test
    public void t3() {
        System.out.println("this is new test");
        Form form=new Form("","成人","s1","国内航班","v1","1",
                "1+ 10 60 10","普通行李","","","","","5000",-1);
        Assert.assertEquals(-1,calc.calculatePrice(form));
    }
    @Test
    public void t4() {
        System.out.println("this is new test");
        Form form=new Form("","成人","s1","国内航班","v1","1",
                "30 30 30 33","普通行李","","","","","5000",-1);
        Assert.assertEquals(-1,calc.calculatePrice(form));
    }
    @Test
    public void t5() {
        System.out.println("this is new test");
        Form form=new Form("","成人","s1","国内航班","v1","1",
                "30 30 30 1","C类运动器械器具","","","","","5000",-1);
        Assert.assertEquals(-1,calc.calculatePrice(form));
    }
    @Test
    public void t6() {
        System.out.println("this is new test");
        Form form=new Form("","成人","s1","国内航班","v1","1",
                "30 30 30 10","普通行李","","","","","100000",-1);
        Assert.assertEquals(-1,calc.calculatePrice(form));
    }
    @Test
    public void t7() {
        System.out.println("this is new test");
        Form form=new Form("r1","儿童","s2","国际航班","v1","3",
                "30 30 30 3","B类运动器械器具","30 30 30 3","C类运动器械器具","30 30 30 3","A类其他特殊行李","5000",5200);
        Assert.assertEquals(5200,calc.calculatePrice(form));
    }
    @Test
    public void t8() {
        System.out.println("this is new test");
        Form form=new Form("r1","婴儿","s3","国际航班","v2","3",
                "30 30 30 3","B类其他特殊行李","30 30 30 3","C类其他特殊行李","30 30 30 3","D类其他特殊行李","5000",3090);
        Assert.assertTrue(true);
    }
    @Test
    public void t9() {
        System.out.println("this is new test");
        Form form=new Form("r1","婴儿","s3","国际航班","v2","3",
                "30 30 30 3","B类其他特殊行李","30 30 30 3","C类其他特殊行李","30 30 30 3","D类其他特殊行李","5000",3090);
        Assert.assertTrue(true);
    }
    @Test
    public void t10() {
        System.out.println("this is new test");
        Form form=new Form("r1","婴儿","s3","国际航班","v2","3",
                "30 30 30 3","B类其他特殊行李","30 30 30 3","C类其他特殊行李","30 30 30 3","D类其他特殊行李","5000",3090);
        Assert.assertTrue(true);
    }
}
